import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';

function FooterHomeOne({ className }) {
   var currentTime = new Date();
   var year = currentTime.getFullYear();

    return (
      <>
        <section
          className={`appie-footer-area appie-footer-3-area ${className}`}
        >
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-6">
                <div className="footer-about-widget footer-about-widget-3">
                  <div className="logo">
                    <a href="#">
                      <img src={logo} alt="" />
                    </a>
                  </div>
                  <p>
                    Bidding Plus Adalah Sebuah.... Bidding Plus Adalah
                    Sebuah.... Bidding Plus Adalah Sebuah.... Bidding Plus
                    Adalah Sebuah.... Bidding Plus Adalah Sebuah....
                  </p>
                  <div className="social mt-30">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="fab fa-facebook-f" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fab fa-twitter" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fab fa-pinterest-p" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fab fa-linkedin-in" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-6">
                <div className="footer-navigation footer-navigation-3">
                  <h4 className="title">Company</h4>
                  <ul>
                    <li>
                      <Link to="/about-us">About Us</Link>
                    </li>
                    <li>
                      <Link to="/Service">Service</Link>
                    </li>
                    <li>
                      <a href="#">Case Studies</a>
                    </li>
                    <li>
                      <Link to="/news">Blog</Link>
                    </li>
                    <li>
                      <Link to="/contact">Contact</Link>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-lg-5 col-md-6">
                <div className="footer-widget-info">
                  <h4 className="title">Get In Touch</h4>
                  <ul>
                    <li>
                      <a href="#">
                        <i className="fal fa-envelope" />{" "}
                        cs.enbmobilecare@gmail.com
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fal fa-phone" /> +(021) 25675466
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fal fa-map-marker-alt" />
                        Komplek Ruko Wijaya Grand Centre Jl. Wijaya II Blok B
                        Persil No. 4, Pulo, Kebayoran Baru, Kota Jakarta
                        Selatan, Daerah Khusus Ibukota Jakarta 12160, Indonesia
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div
                  className="
                footer-copyright
                d-flex
                align-items-center
                justify-content-between
                pt-35
              "
                >
                  <div className="apps-download-btn">
                    <ul>
                      <li>
                        <a href="https://apps.apple.com/id/app/bidding-plus/id1597057710?l=id">
                          <i className="fab fa-apple" /> Download for iOS
                        </a>
                      </li>
                      <li>
                        <a
                          className="item-2"
                          href="https://play.google.com/store/apps/details?id=id.biddingplus&hl=en_US&gl=id"
                        >
                          <i className="fab fa-google-play" /> Download for
                          Android
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="copyright-text">
                    <p>Copyright {year} BiddingPlus. All rights reserved.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
}

export default FooterHomeOne;
