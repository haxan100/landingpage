import React from 'react';

function Forms() {
    return (
      <>
        <section className="contact-section">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="contact--info-area">
                  <h3>Get in touch</h3>
                  <p>Mau Meminta Bantuasn ?.</p>
                  <div className="single-info">
                    <h5>Headquaters</h5>
                    <p>
                      <i className="fal fa-home"></i>
                      Komplek Ruko Wijaya Grand Centre Jl. Wijaya II Blok B <br />
                      Persil No. 4, Pulo, Kebayoran Baru, Kota Jakarta Selatan,<br />
                      Daerah Khusus Ibukota Jakarta 12160, Indonesia<br />
                    </p>
                  </div>
                  <div className="single-info">
                    <h5>Phone</h5>
                    <p>
                      <i className="fal fa-phone"></i>
                      (+6221) 25675466
                    </p>
                  </div>
                  <div className="single-info">
                    <h5>Support</h5>
                    <p>
                      <i className="fal fa-envelope"></i>
                      cs.enbmobilecare@gmail.com
                    </p>
                  </div>
                  <div className="ab-social">
                    <h5>Follow Us</h5>
                    <a className="fac" href="#">
                      <i className="fab fa-facebook-f"></i>
                    </a>
                    <a className="twi" href="#">
                      <i className="fab fa-twitter"></i>
                    </a>
                    <a className="you" href="#">
                      <i className="fab fa-youtube"></i>
                    </a>
                    <a className="lin" href="#">
                      <i className="fab fa-linkedin-in"></i>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-8">
                <div className="bisylms-map">
                  <iframe
                    title="map"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126926.8143622008!2d106.69262295268244!3d-6.202496701746204!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1d5563bc713%3A0x1e6e008adee8b07e!2sENB%20MOBILE%20CARE!5e0!3m2!1sid!2sid!4v1655798290807!5m2!1sid!2sid&amp;ie=UTF8&amp;t=p&amp;z=16&amp;iwloc=B&amp;output=embed"
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
}

export default Forms;
