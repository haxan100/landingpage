import React from 'react';
import aboutThumb from '../../assets/images/about-thumb-2.png';
import aboutThumb3 from '../../assets/images/about-thumb-3.png';
import icon1 from '../../assets/images/icon/1.png';
import icon5 from '../../assets/images/icon/5.svg';
import icon6 from '../../assets/images/icon/6.svg';
import icon7 from '../../assets/images/icon/7.svg';

function AboutHomeThree() {
    return (
      <>
        <section className="appie-about-3-area pt-100 pb-100" id="features">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-6">
                <div
                  className="appie-about-thumb-3 wow animated fadeInLeft"
                  data-wow-duration="2000ms"
                  data-wow-delay="400ms"
                >
                  <img
                    src={"https://www.laku6.com/images/29e403d.webp"}
                    alt=""
                  />
                </div>
              </div>
              <div className="col-lg-6">
                <div className="appie-traffic-title">
                  <h3 className="title">
                    Marketplace Yang Memberikan Anda Penawaran Menarik 
                  </h3>
                  <div className="col-lg- pt-3 pt-lg-0 content">
                    <p className='textTengah'>
                      BiddingPlus Memberikan Anda Produk Produk Elektronik Yang Menarik Dari Segi Harga Dan Kualitas 
                    </p>
                    <p className='textBawah'>
                      Beberapa keunggulan menjual kepada para pelaku bisnis:
                    </p>
                    <ul>
                      <li>
                        <img
                          src={
                            "https://cdn3.iconfinder.com/data/icons/chat-and-messeger-app/64/21-_double-checklist-s_eeen-_approve-512.png"
                          }
                          className="logoCekList"
                          alt=""
                        />
                        <i className="bx bx-check-double"></i>
                        <b>Pasti Terjual</b>, ribuan pembeli memiliki banyak
                        daftar barang yang mereka inginkan
                      </li>
                      <li>
                        <img
                          src={
                            "https://cdn3.iconfinder.com/data/icons/chat-and-messeger-app/64/21-_double-checklist-s_eeen-_approve-512.png"
                          }
                          className="logoCekList"
                          alt=""
                        />
                        <i className="bx bx-check-double"></i>
                        <b>Kecepatan</b>, ribuan pembeli di platform kami siap
                        membeli tanpa negosiasi
                      </li>
                      <li>
                        <img
                          src={
                            "https://cdn3.iconfinder.com/data/icons/chat-and-messeger-app/64/21-_double-checklist-s_eeen-_approve-512.png"
                          }
                          className="logoCekList"
                          alt=""
                        />
                        <i className="bx bx-check-double"></i>
                        <b>Aman dan Mudah</b>, kami menyediakan layanan seperti
                        penjemputan dari penjual, pengecekan kondisi, dan
                        pengantaran ke pembeli
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
}

export default AboutHomeThree;
