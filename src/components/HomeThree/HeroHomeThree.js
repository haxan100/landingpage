import React, { useState } from 'react';
import PopupVideo from '../PopupVideo';

function HeroHomeThree() {
    const [showVideo, setVideoValue] = useState(false);
    const handleShowVideo = (e) => {
        e.preventDefault();
        setVideoValue(!showVideo);
    };
    return (
        <>
            {showVideo && (
                <PopupVideo
                    videoSrc="//www.youtube.com/embed/EE7NqzhMDms?autoplay=1"
                    handler={(e) => handleShowVideo(e)}
                />
            )}
            <section className="appie-hero-area appie-hero-3-area">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-10">
                            <div className="appie-hero-content text-center">
                                <h1 className="appie-title">Platform ReCommerce Terpercaya di Indonesia</h1>
                                <p>Kami Mengapresiasi Anda Karna Percaya Kepada Platform Kami Untuk Menjual Beli Produk Handphone Anda
                                </p>
                                <div
                                    className="thumb mt-100 wow animated fadeInUp"
                                    data-wow-duration="2000ms"
                                    data-wow-delay="400ms"> 
                                    <div className="row justify-content-center sectionHeader">
                                        <div className="col-lg-11">
                                            <div className="row justify-content-end section">

                                                <div className="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                                                    <div className="count-box py-5 d-md-flex">
                                                        <div className="pb-2 pr-2">
                                                             <img src={'https://www.downloadclipart.net/large/sold-png-photo.png'} alt="" className="img-responsive" />
                                                        </div>
                                                        <div className="text-lg-left text-md-left text-center">
                                                            <span>500,000</span>
                                                            <p>Gadget Terjual</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                                                    <div className="count-box py-5 d-md-flex">
                                                        <div className="pb-2 pr-2">
                                                            <img src={'https://www.downloadclipart.net/large/sold-png-photo.png'} alt="" className="img-responsive" />

                                                        </div>
                                                        <div className="text-lg-left text-md-left text-center">
                                                            <span>$50 Million</span>
                                                            <p>Diteruskan kepada para Penjual</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                                                    <div className="count-box pb-5 pt-0 pt-lg-5 d-md-flex">
                                                        <div className="pb-2 pr-2">
                                                             <img src={'https://www.downloadclipart.net/large/sold-png-photo.png'} alt="" className="img-responsive" />
                                                        </div>
                                                        <div className="text-lg-left text-md-left text-center">
                                                            <span>&gt;1,000</span>
                                                            <p>Lokasi tersebar di seluruh Indonesia</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                                                    <div className="count-box pb-5 pt-0 pt-lg-5 d-md-flex">
                                                        <div className="pb-2 pr-2">
                                                             <img src={'https://www.downloadclipart.net/large/sold-png-photo.png'} alt="" className="img-responsive" />
                                                        </div>
                                                        <div className="text-lg-left text-md-left text-center">
                                                            <span>&gt;5,000</span>
                                                            <p>Pelaku Bisnis sebagai pembeli</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default HeroHomeThree;
