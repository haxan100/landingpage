import React, { useEffect, useRef, useState } from 'react';
import Slider from 'react-slick';
import SimpleReactLightbox, { SRLWrapper } from 'simple-react-lightbox';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import axios from "axios";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';



function ShowCaseHomeThree() {
  
    function imei_gen() {
      var pos;
      var str = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      var sum = 0;
      var final_digit = 0;
      var t = 0;
      var len_offset = 0;
      var len = 15;
      var rbi = [
        "01",
        "10",
        "30",
        "33",
        "35",
        "44",
        "45",
        "49",
        "50",
        "51",
        "52",
        "53",
        "54",
        "86",
        "91",
        "98",
        "99",
      ];
      var arr = rbi[Math.floor(Math.random() * rbi.length)].split("");
      str[0] = Number(arr[0]);
      str[1] = Number(arr[1]);
      pos = 2;
      while (pos < len - 1) {
        str[pos++] = Math.floor(Math.random() * 10) % 10;
      }
      len_offset = (len + 1) % 2;
      for (pos = 0; pos < len - 1; pos++) {
        if ((pos + len_offset) % 2) {
          t = str[pos] * 2;
          if (t > 9) {
            t -= 9;
          }
          sum += t;
        } else {
          sum += str[pos];
        }
      }
      final_digit = (10 - (sum % 10)) % 10;
      str[len - 1] = final_digit;
      t = str.join("");
      t = t.substr(0, len);
      console.log(t)
      return t;
    }

    const responsive = {
      superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
      }
    };
   
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true);

    useEffect(() => {
      fetchData();    
    }, [])
    
    const fetchData = async () =>{
        setLoading(true);
      try {
        const config = {
          headers: {
            "Content-Type": "application/json",
          },
        };
        const { data: response } = await axios.get(
          "https://dev.tradeinplus.id/bidding-1/api/slider",
          [],
          config
        );
        setData(response);
        setLoading(false);
      } catch (error) {
        console.error(error.message);
      }
    }

    return (
      <>
        <section className="appie-showcase-area">
          <SimpleReactLightbox>
            <SRLWrapper>
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="appie-section-title text-center">
                      <h3 className="appie-title">Promo</h3>
                      <p className="textPromo">Lihat Promo Menarik Kami.</p>
                    </div>
                  </div>
                </div>
                <div className="row appie-showcase-slider">
                  <div className="col-lg-12">
                    <Carousel
                      showDots={true}
                      responsive={responsive}
                      ssr={true} // means to render carousel on server-side.
                      infinite={true}
                      autoPlay={true}
                      autoPlaySpeed={1990}
                      keyBoardControl={true}
                      customTransition="all .5"
                      transitionDuration={800}
                      containerClass="carousel-container"
                      removeArrowOnDeviceType={["tablet", "mobile"]}
                      dotListClass="custom-dot-list-style"
                      itemClass="carousel-item-padding-40-px"
                    >
                      {!loading &&
                        data.data.map((datanya, key) => {
                          return (
                            <div
                              key={datanya.id_promo}
                              className="appie-showcase-item mt-30"
                              onClick={() => imei_gen()}
                            >
                              <a className="appie-image-popup">
                                <img
                                  src={`https://dev.tradeinplus.id/bidding-1/assets/uploads/promo/${datanya.foto}`}
                                  alt=""
                                />
                              </a>
                            </div>
                          );
                        })}
                    </Carousel>
                  </div>
                </div>
              </div>
            </SRLWrapper>
          </SimpleReactLightbox>
        </section>
      </>
    );
}

export default ShowCaseHomeThree;
