import React from 'react';
import { Link } from 'react-router-dom';
import BlogFour from '../../assets/images/blog-4.jpg';
import BlogFive from '../../assets/images/blog-5.jpg';
import BlogSix from '../../assets/images/blog-6.jpg';
import BlogSeven from '../../assets/images/blog-7.jpg';

function BlogHomeThree() {
    return (
        <>
            <section className="appie-blog-3-area pt-90 pb-100">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="appie-section-title text-center">
                                <h3 className="appie-title">Layanan Kami</h3>
                                <p>Keseluruhan Layanan Kami.</p>
                            </div>
                        </div>
                    </div>
                    <div className="row services">

                        <div className="col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <div className="icon-box">
                                <div className="icon">
                                    <img className='img-responsive gambarLayananKami' src={'https://menu.tradeinplus.id//view/img/ic_portalweb.png'} alt="" />                                    
                                </div>
                                <div className="icon-box-text">
                                    <h4 className="title">
                                        <a href="https://play.google.com/store/apps/details?id=com.biddingnativeapp" className='tulisan'  target="_blank">BiddingPlus Regular</a></h4>
                                    <p className="description">Bidding Menawarkan Produk Handphone Yang Berkualitas Dari Segi Produk Dan Harga Yang Bersaing
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <div className="icon-box">
                                <div className="icon">
                                    <img className='img-responsive gambarLayananKami' src={'https://menu.tradeinplus.id//view/img/ic_portalweb.png'} alt="" />                                    
                                </div>
                                <div className="icon-box-text">
                                    <h4 className="title"><a href="https://play.google.com/store/apps/details?id=com.biddingnativeapp"  className='tulisan'  target="_blank">BiddingPlus Elektronik</a></h4>
                                    <p className="description">Bidding Menawarkan Produk Elektronik Seperti Laptop, Smartwatch,Dekstop Dll Yang Berkualitas Dari Segi Produk Dan Harga Yang Bersaing
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <div className="icon-box">
                                <div className="icon">
                                    <img className='img-responsive gambarLayananKami' src={'https://menu.tradeinplus.id//view/img/ic_portalweb.png'} alt="" />                                    
                                </div>
                                <div className="icon-box-text">
                                    <h4 className="title"><a href="https://play.google.com/store/apps/details?id=com.biddingnativeapp"  className='tulisan'  target="_blank">BiddingPlus Bundling</a></h4>
                                    <p className="description">Bidding Bundling Adalah Produk-Produk Bidding Yang Dari Paket Untuk Di Jualkan Kepada Anda
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <div className="icon-box">
                                <div className="icon">
                                    <img className='img-responsive gambarLayananKami' src={'https://menu.tradeinplus.id//view/img/ic_portalweb.png'} alt="" />                                    
                                </div>
                                <div className="icon-box-text">
                                    <h4 className="title"><a href="https://play.google.com/store/apps/details?id=com.biddingnativeapp"  className='tulisan'  target="_blank">BiddingPlus Live Demo Unit</a></h4>
                                    <p className="description">Bidding LDU  Adalah Produk-Produk Bidding Dari Live Demo Unit Untuk Di Jualkan Kepada Anda
                                    </p>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </section>
        </>
    );
}

export default BlogHomeThree;
