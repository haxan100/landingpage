import React from 'react';
import { Link } from 'react-router-dom';

function Navigation() {
    return (
        <>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/about-us">Tentang Kami</Link>
                </li>
                <li>
                    <Link to="/service">Cek Harga</Link>
                </li>
                <li>
                    <Link to="/contact">Kontak</Link>
                </li>
                {/* <li>
                    <Link to="/contact">Promosi</Link>
                </li>
                <li>
                    <Link to="/contact">Karir</Link>
                </li> */}
            </ul>
        </>
    );
}

export default Navigation;
