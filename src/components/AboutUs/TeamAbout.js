import React, { useEffect, useState } from 'react';
import team4 from '../../assets/images/team-4.jpg';
import axios from "axios";


function TeamAbout() {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    var url = "https://dev.tradeinplus.id/bidding-1/api/listGrade";

    useEffect(() => {
      fetchData();
    }, []);

    const fetchData = async () => {
      setLoading(true);
      try {
         const dataPost = { id_tipe_produk: 1 };
         axios
           .post(url, dataPost)
           .then((response) => {
               setData(response.data);
               setLoading(false);
            });
        } catch (error) {
            console.error(error.message);
        }
    };
    
    return (
        
        <>
            <section className="appie-team-area appie-team-about-area pb-90">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="appie-section-title text-center">
                                <h3 className="appie-title">Grade On Our Product</h3>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {!loading && (
                            data.data.map((d)=>{
                                return (
                                  <div className="col-lg-3 col-md-6">
                                    <div
                                      className="appie-team-item appie-team-item-about mt-30 wow animated fadeInUp"
                                      data-wow-duration="2000ms"
                                      data-wow-delay="800ms"
                                    >
                                      <div className="thumb">
                                        <img src={team4} alt="" />
                                      </div>
                                      <div className="content text-left">
                                        <h2 className="title text-center">{d.grade}</h2>                                        
                                      </div>
                                    </div>
                                  </div>
                                );
                            })
                        )}
                    </div>
                </div>
            </section>
        </>
    );
}

export default TeamAbout;
