import React from 'react';

function HeroAbout() {
    return (
        <>
            <div className="appie-page-title-area appie-page-service-title-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7">
                            <div className="appie-about-top-title">
                                <h2 className="title titleBidingPlus">Bidding Plus Ada untuk Anda</h2>
                                <p className='textTawarkan'>Kita Tawarkan HanPhone Berkualitas Baik untuk Anda .</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section className="appie-about-page-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="appie-about-page-content">
                                <h3 className="title">
                                    Kami yakin bahwa membeli dan menjual gadget seharusnya aman, mudah dan dengan standar yang jelas.
                                </h3>
                                <p>
                                   Oleh karena itu kami mem ulai Laku6.com pada tahun 2016 dengan menyediakan informasi yang akurat dan terkini dalam hal Harga Gadget Preloved, sehingga Anda akan selalu mengetahui harga yang tepat saat ingin menjual gadget Anda, begitu pula agar Anda tidak salah harga saat ingin membeli gadget.

                                Jika Anda adalah individu yang hanya ingin menjual gadget preloved tanpa membeli yang baru, kami punya solusi yang sangat mudah, cepat, dan menguntungkan, yaitu Maujual dan Langsung Laku by Tokopedia.

                                Sedangkan jika Anda adalah individu yang memiliki kebutuhan membeli gadget baru dan saat ini masih memiliki gadget, kami dapat membantu Anda dengan solusi Trade-In (Tukar Tambah) di ribuan titik pelayanan di seluruh Indonesia, secara Online maupun Offline.

                                Selain itu apabila Anda adalah pelaku bisnis gadget kecil atau menengah, kami menyediakan platform Laku6 Trade Center (LTC) yang menggunakan metode bidding digital yang mudah, aman, dan terpercaya.

                                Tujuan kami sebagai sebuah platform adalah untuk membantu Anda menjual dan membeli gadget preloved yang tepat dengan harga yang sesuai dan mendapatkan pengalaman terbaik.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default HeroAbout;
